#!/usr/bin/env python

from Bio import SeqIO
import sys

infile=sys.argv[1]
infile2=sys.argv[2]

res=dict()
with open(infile2,"r") as ff:
    for line in ff:
        line=line.rstrip()
        parts=line.split("\t")
        res[parts[0]]=parts[1]

with open(infile,"r") as f:
    for line in f:
        line=line.rstrip()
        if(line.startswith(">")):
            p=line.split(" ")
            id=p[0].replace(">","")
            try:
                print(">"+res[id])
            except KeyError:
                print("ERROR")
                print(id)
                exit(1)
        else:
            print(line)

