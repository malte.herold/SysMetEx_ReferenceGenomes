# Leptospirillum ferriphilum DSM14647

Resequenced genome

2 Contigs: 1 Chromosome, 1 short contig

Short Contig might be disregarded in the future as it is likely not a plasmid


``




## Details for annotation:

https://git-r3lab.uni.lu/malte.herold/LF_annotation

files lf_merged. annotated with prokka with genus db of leptospirillum genomes available on IMG:

https://git-r3lab.uni.lu/malte.herold/SysMetEx_ReferenceGenomes/issues/1